import random
# Parametros
MAXLISTA = 10
INDICEINICIAL = 0

# Populando a lista base
def populaLista(lista, tamanho, indice):
    if(indice == tamanho):
        return
    else:
        lista.append(random.randint(-1000,1000))
        return populaLista(lista, tamanho, indice+1)

# Atv 1
def multiplicandoPor2 (lista, indice, listanova=[]):
    if(indice == MAXLISTA):
        return listanova
    else:
        listanova.append(lista[indice] * 2)
        return multiplicandoPor2 (lista, indice+1, listanova)

# Atv 2
def mediaDosNumeros (lista, soma, indice):
    if(indice == MAXLISTA):
        return (soma/(len(lista)))
    else:
        return mediaDosNumeros (lista, (soma + lista[indice]), indice+1)

# Atv 3 merge-sort

def metade(lista):
  meio = int(len(lista)/2)
  return lista[:meio], lista[meio:]

def merge(L,R,topoL,topoR,saida):
  if (len(L) != topoL and len(R) != topoR): 
    if (L[topoL] <= R[topoR]):
      saida.append(L[topoL])
      return merge(L,R,topoL+1,topoR,saida)
    else:
      saida.append(R[topoR])
      return merge(L,R,topoL,topoR+1,saida)
  if (len(L) != topoL):
    saida.append(L[topoL])
    return merge(L,R,topoL+1,topoR,saida)
  if (len(R) != topoR):
    saida.append(R[topoR])
    return merge(L,R,topoL,topoR+1,saida)
  return saida 

def mergesort(lista):
  if (len(lista) <= 1):
    return lista
  esquerda, direita = metade(lista)
  L = mergesort(esquerda)
  R = mergesort(direita)
  saida=[]
  return merge(L,R,0,0,saida)

# atv 4
def separaLetrasApenas(string, lista=[],  inicio=0, fim=None):
  if (fim == None):
    fim = len(string)
  if(inicio == fim):
    return lista
  char = converteCaps(string[inicio])
  if(char != None):
    lista.append(char)
  return separaLetrasApenas(string, lista, inicio+1, fim)

def converteCaps(char):
  if(char == 'a' or char == 'A'):
    return 'a'
  if(char == 'b' or char == 'B'):
    return 'b'
  if(char == 'c' or char == 'C'):
    return 'c'
  if(char == 'd' or char == 'D'):
    return 'd'
  if(char == 'e' or char == 'E'):
    return 'e'
  if(char == 'f' or char == 'F'):
    return 'f'
  if(char == 'g' or char == 'G'):
    return 'g'
  if(char == 'h' or char == 'H'):
    return 'h'
  if(char == 'i' or char == 'I'):
    return 'i'
  if(char == 'j' or char == 'J'):
    return 'j'
  if(char == 'k' or char == 'K'):
    return 'k'
  if(char == 'l' or char == 'L'):
    return 'l'
  if(char == 'm' or char == 'M'):
    return 'm'
  if(char == 'n' or char == 'N'):
    return 'n'
  if(char == 'o' or char == 'O'):
    return 'o'
  if(char == 'p' or char == 'P'):
    return 'p'
  if(char == 'q' or char == 'Q'):
    return 'q'
  if(char == 'r' or char == 'R'):
    return 'r'
  if(char == 's' or char == 'S'):
    return 's'
  if(char == 't' or char == 'T'):
    return 't'
  if(char == 'u' or char == 'U'):
    return 'u'
  if(char == 'v' or char == 'V'):
    return 'v'
  if(char == 'w' or char == 'W'):
    return 'w'
  if(char == 'x' or char == 'X'):
    return 'x'
  if(char == 'y' or char == 'y'):
    return 'y'
  if(char == 'z' or char == 'Z'):
    return 'z'
  if(char == '0'):
    return '0' 
  if(char == '1'):
    return '1'
  if(char == '2'):
    return '2'
  if(char == '3'):
    return '3'
  if(char == '4'):
    return '4'
  if(char == '5'):
    return '5'
  if(char == '6'):
    return '6'
  if(char == '7'):
    return '7'
  if(char == '8'):
    return '8'
  if(char == '9'):
    return '9' 
  return None

def inverteString (frase, lista=[], fim=None):
  if(fim == None):
    fim = len(frase)-1
  if(fim >= 0):
    lista.append(frase[fim])
    return inverteString(frase, lista, fim-1)
  return lista

def testaPalindromo(string1, string2, inicio=0, fim=None):
  if(fim == None):
    fim = len(string1)
  if(inicio == fim):
    return print("É um palindromo!")
  if(string1[inicio] == string2[inicio]):
    return testaPalindromo(string1, string2, inicio+1, fim)
  else:
    return print("Não é um palindromo!")

# Uso das tarefas
listaBase = []
populaLista(listaBase,MAXLISTA,INDICEINICIAL)
print("Lista inicial: ", listaBase)
# 1
listaMultiplicadaPor2 = multiplicandoPor2(listaBase,INDICEINICIAL)
print("Multiplicada por 2: ", listaMultiplicadaPor2)
# 2
print("A média deles é: ", mediaDosNumeros (listaBase, 0, INDICEINICIAL))
# 3
listaOrdenada = mergesort(listaBase)
print("Números ordenados: ", listaOrdenada)
# 4
stringbase = input("Digite uma palavra ou frase: ")
string = separaLetrasApenas(stringbase)
gnirts = inverteString(string)
testaPalindromo(string,gnirts)