# paradigmaFuncionalEmPy

Realizado Como atividade durante a disciplina de "Linguagens de Programação", 2/21.

Utilizando uma linguagem de programação que utiliza o paradigma funcional desenvolva as seguintes soluções:

1) Dado como entrada uma array com 1000 números inteiros, crie um programa que multiplica todos os valores por 2 e salva em um novo array;

2) Dado como entrada uma array com 1000 números inteiros, crie um programa que apresente a média dos valores;

3) Implemente o algoritmo de ordenação com o Merge-Sort. Informações sobre esse algoritmo podem ser obtidas em https://joaoarthurbm.github.io/eda/posts/merge-sort/

4) Utilizando recursividade, verifique se uma string é um palíndromo. Dicas, desconsiderar da análise caracteres não-numéricos;

Procure utilizar recursos funcionais que a linguagem de programação já possua. Para os enunciados 3 e 4, não utilize funções prontas ou bibliotecas que verificam se uma string é palíndromo ou para fazer o Merge-Sort.

